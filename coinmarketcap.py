from bs4 import BeautifulSoup
import requests
import csv
import time
import coinmarketcap_url as cmc_url
import common

csv_t_list = []
csv_v_list = []


def run():
    print('Start..')
    url = generate_url_to_search(cmc_url.token_list)  # 產生本次所有要搜尋的 url
    for i in range(len(url)):  # 每個 url 都 run
        r = requests.get(url[i])
        if r.status_code != 200:
            common.http_status_not_200(r)
            continue
        else:
            coin_market_cap_data(r)
    export_csv()


def coin_market_cap_data(r):
    r.encoding = "utf8"
    soup = BeautifulSoup(r.text, "html.parser")
    # print(soup.prettify())  # 美化html

    token_name = soup.find("h2")  # 找該token的名稱
    title_list = ["Tokens"]  # 資料標題的list, 第一格名稱為 'token'
    column_list = [token_name.text]  # 資料內容的 list, token 的名稱

    all_title = soup.findAll("div", class_="sc-16r8icm-0 cEbjrm statsLabel")  # 尋找所有的資料標題
    all_value = soup.findAll("div", class_="statsValue")  # 尋找所有的資料內容
    market_cap_index = 0
    for t in all_title:
        if t.get_text() == "Market Cap":  # 確認為 Market Cap 的資料才會放入value_list
            title_list.append(t.get_text())
            market_cap_index = title_list.index("Market Cap")  # 紀錄 Market Cap 在 List 的位置
            column_list.append(all_value[market_cap_index - 1].get_text())
        if t.get_text() == "Fully Diluted Market Cap":
            title_list.append(t.get_text())
            market_cap_index = title_list.index("Fully Diluted Market Cap")  # 紀錄 Fully Diluted Market Cap 在 List 的位置
            column_list.append(all_value[market_cap_index].get_text())
    csv_t_list.append(title_list)
    csv_v_list.append(column_list)


def generate_url_to_search(url_list):
    t_url = []
    for i in range(len(url_list)):  # 查找token_list的每一個項目
        if cmc_url.token_list[i] == "":  # 若項目中資料是空的則不進行"合併網址"
            continue
        else:
            url = cmc_url.currencies_url + cmc_url.token_list[i]  # 進行"合併網址"
            t_url.append(url)  # 將合併後的網址塞入t_url中
    return t_url


def export_csv():
    csv_file = "Market Cap_" + time.strftime("%Y%m%d_%H%M", time.localtime()) + ".csv"
    with open(csv_file, 'w+', newline='') as fp:
        writer = csv.writer(fp)
        writer.writerow(csv_t_list[0])
        for i in range(len(csv_t_list)):
            writer.writerow(csv_v_list[i])
