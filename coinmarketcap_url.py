currencies_url = "https://coinmarketcap.com/currencies/"
AR_url = "arweave/"
STX_url = "stacks/"
RUNE_url = "thorchain/"
SCRT_url = "secret/"
YFI_url = "yearn-finance/"
RLY_url = "rally/"
ZNN_url = "zenon/"
RGT_url = "rari-governance-token/"
RIF_url = "rsk-infrastructure-framework/"
SOV_url = "sovryn/"
SILO_url = "silo-finance/"
INST_url = "instadapp/"

token_list = [
    AR_url,
    STX_url,
    RUNE_url,
    SCRT_url,
    YFI_url,
    RLY_url,
    ZNN_url,
    RGT_url,
    RIF_url,
    SOV_url,
    SILO_url,
    INST_url
]
